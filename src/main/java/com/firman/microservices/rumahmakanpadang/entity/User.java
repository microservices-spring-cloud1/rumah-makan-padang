package com.firman.microservices.rumahmakanpadang.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "users")
public class User {
    @Id
    @SequenceGenerator(name="user_seq", sequenceName="user_seq", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator= "user_seq")
    private Long id;

    @NotBlank
    @Column(unique = true)
    @Size(min = 1, max = 100)
    private String username;

    @NotBlank
    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;
}
