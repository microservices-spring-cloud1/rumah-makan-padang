package com.firman.microservices.rumahmakanpadang.repository;

import com.firman.microservices.rumahmakanpadang.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
