package com.firman.microservices.rumahmakanpadang;

import com.firman.microservices.rumahmakanpadang.entity.User;
import com.firman.microservices.rumahmakanpadang.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@SpringBootApplication
@RestController
@EnableFeignClients
@EnableCircuitBreaker
public class RumahMakanPadangApplication {

	public static void main(String[] args) {
		SpringApplication.run(RumahMakanPadangApplication.class, args);
	}

	@Autowired
	UserRepository userRepository;

	@GetMapping("/hello/{id}")
	public ResponseEntity<?> hello(@PathVariable Long id){
		if(id != null){
			Optional<User> user = userRepository.findById(id);
			if(user.isPresent()){
				return new ResponseEntity<String>("Hello, "+user.get().getFirstName()+" "+user.get().getLastName(), HttpStatus.OK);
			}
		}
		return new ResponseEntity<String>("Hello World", HttpStatus.OK);
	}

}
