package com.firman.microservices.rumahmakanpadang.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "client-service-1", fallback = ClientService1Fallback.class)
@RibbonClient(name="client-service-1")
public interface ClientService1 {
    @RequestMapping(method = RequestMethod.GET, value = "/hello/1")
    public String getHello();
}
