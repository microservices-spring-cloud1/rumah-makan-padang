package com.firman.microservices.rumahmakanpadang.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ClientService1Fallback implements ClientService1{
    @Override
    public String getHello() {
        log.info("Backend service offline, menjalankan fallback");
        return "Backend service offline, menjalankan fallback";
    }
}
